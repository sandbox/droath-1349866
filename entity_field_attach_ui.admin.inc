<?php

/**
 * @file,
 * Defines the admin implementations for entity field attach UI.
 */

/**
 * Menu callback; Displays the admin UI for the current field.
 */
function entity_field_attach_ui_display($form, $form_state, $entity_type, $bundle, $field_name) {

  // Load the include file for a specific entity UI.
  $load_entity_ui_file = module_load_include('inc', 'entity_field_attach_ui', "ui/$entity_type");
  // Retrieve the bundle type for the current field.
  $bundle_type = field_extract_bundle($entity_type, $bundle);
  // Load the entity field attach UI settings.
  $field_settings = entity_field_attach_ui_load($entity_type, $bundle_type, $field_name);

  $form['#tree'] = TRUE;

  // Field UI settings.
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Field Settings',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['settings']['specific'] = array();

  // Invoke hook_ENTITY_TYPE_field_attach_ui().
  $specific_settings = module_invoke_all($entity_type . '_field_attach_ui', $bundle, $field_name);
  if (!empty($specific_settings)) {
    $form['settings']['specific'] += $specific_settings;
  }
  else {
    $form['settings']['specific'] = array(
      '#markup' => t('No specific field UI settings have been defined for this entity type.'),
    );
  }

  // Field common settings.
  $form['settings']['common'] = array(
    '#type' => 'fieldset',
    '#title' => 'Common Settings',
    '#description' => t('Here you will find common field UI settings that are not entity or bundle specific.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['settings']['common']['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => isset($field_settings['label']) ? $field_settings['label'] : NULL,
  );
  $form['settings']['common']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Help text'),
    '#description' => t('Instructions to present to the user below this field on the editing form.<br/> 
    Allowed HTML tags: &lt;a&gt; &lt;b&gt; &lt;big&gt; &lt;code&gt; &lt;del&gt; &lt;em&gt; &lt;i&gt; &lt;ins&gt; &lt;pre&gt; &lt;q&gt; &lt;small&gt; &lt;span&gt; &lt;strong&gt; &lt;sub&gt; &lt;sup&gt; &lt;tt&gt; &lt;ol&gt; &lt;ul&gt; &lt;li&gt; &lt;p&gt; &lt;br&gt; &lt;img&gt;'),
    '#rows' => 5,
    '#default_value' => isset($field_settings['description']) ? $field_settings['description'] : NULL,
  );

  // Field attach visibility settings.
  $form['visibility'] = array(
    '#type' => 'fieldset',
    '#title' => 'Field Visibility',
    '#description' => t('Allows you to configure the visibility settings for this field. NOTE: Not all entity fields support this feature (e.g., title, name).'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['visibility']['hidden'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hidden Field'),
    '#description' => t("If selected, this field won't be visible on the entity edit form."),
    '#default_value' => isset($field_settings['hidden']) ? $field_settings['hidden'] : NULL,
  );

  // Disable the hidden field feature.
  $hidden_field_disable = module_invoke_all($entity_type . '_hidden_field_disable');
  if (in_array($field_name, $hidden_field_disable)) {
    $form['visibility']['hidden']['#disabled'] = TRUE;
  }

  $form['is_new'] = array(
    '#type' => 'value',
    '#value' => !isset($field_settings['id']) ? TRUE : FALSE,
  );
  // Send along the id if the data is not new.
  if (isset($field_settings['id'])) {
    $form['id'] = array(
      '#type' => 'value',
      '#value' => $field_settings['id'],
    );
  }

  // Field attach actions.
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Field Settings'),
  );
  $form['#entity_type'] = $entity_type;
  $form['#bundle_type'] = $bundle_type;
  $form['#extra_field'] = $field_name;
  return $form;
}

/**
 * The submission hanlder for the field UI display form.
 */
function entity_field_attach_ui_display_submit($form, &$form_state) {
  $bundle_type = $form_state['complete form']['#bundle_type'];
  $extra_field = $form_state['complete form']['#extra_field'];
  $entity_type = $form_state['complete form']['#entity_type'];

  $settings = array();

  if ($data = $form_state['values']) {

    // Defined the base setting instances.
    $settings += $data['visibility'];
    $settings += $data['settings']['common'];

    // Defined if the data entry is new.
    // If its not, then send the entry id.
    $settings['is_new'] = $data['is_new'];
    if (!$settings['is_new']) {
      $settings['id'] = $data['id'];
    }

    // Defined the specific settings.
    if (isset($data['settings']['specific'])) {
      $settings['data'] = serialize($data['settings']['specific']);
    }

    // Save the field attach UI settings.
    entity_field_attach_ui_save($settings, $entity_type, $bundle_type, $extra_field);

    // Retrieve the admin path for this entity/bundle.
    // Then redirect the user back to the fields UI form.
    $path = _field_ui_bundle_admin_path($entity_type, $bundle_type);
    $form_state['redirect'] = $path . '/fields';
  }
}

/**
 * The delete hanlder for the field UI display form.
 */
function entity_field_attach_ui_delete_confirm($form, &$form_state, $entity_type, $bundle, $field_name) {

  // Retrieve the bundle type for the current field.
  $bundle_type = field_extract_bundle($entity_type, $bundle);
  // Retrieve the admin path for the bundle fields UI form.
  $path = $form_state['#redirect_path'] = _field_ui_bundle_admin_path($entity_type, $bundle_type) . '/fields';

  $form_state['#entity_type'] = $entity_type;
  $form_state['#bundle_type'] = $bundle_type;
  $form_state['#field_name'] = $field_name;

  return confirm_form($form,
    t('Are you sure you want to delete these field UI settings?'),
    $path,
    t("This action doesn't remove the field, it only removes the field UI settings. This action cannot be undone."),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * The delete confirmation hanlder for the field UI display form.
 */
function entity_field_attach_ui_delete_confirm_submit($form, &$form_state) {
  $entity_type = $form_state['#entity_type'];
  $bundle_type = $form_state['#bundle_type'];
  $field_name  = $form_state['#field_name'];

  // Delete the field UI settings.
  entity_field_attach_ui_delete($entity_type, $bundle_type, $field_name);
  // Redirect the user back to the fields UI form.
  $form_state['redirect'] = $form_state['#redirect_path'];
}

