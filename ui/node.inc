<?php

/**
 * @file,
 * Defines the node setting form for entity field attach UI.
 */

/**
 * Implements hook_ENTITY_TYPE_field_attach_ui().
 */
function entity_field_attach_ui_node_field_attach_ui($bundle, $field_name) {
  $form = array();
  // Form API code goes here...  
  return $form;
}

/**
 * Implements hook_ENTITY_TYPE_hidden_field_disable().
 */
function entity_field_attach_ui_node_hidden_field_disable() {
  return array(
    'title'
  );
}
