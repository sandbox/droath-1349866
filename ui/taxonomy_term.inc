<?php 

/**
 * Defines the taxonomy term setting form for entity field attach UI.
 */

/**
 * Implements hook_ENTITY_TYPE_field_attach_ui().
 */
function entity_field_attach_ui_taxonomy_term_field_attach_ui($bundle, $field_name) {
  $form = array();
  // Form API code goes here...
  return $form;
}

/**
 * Implements hook_ENTITY_TYPE_hidden_field_disable().
 */
function entity_field_attach_ui_taxonomy_term_hidden_field_disable() {
  return array(
    'name'
  );
}