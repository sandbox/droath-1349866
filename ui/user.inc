<?php

/**
 * Defines the user setting form for entity field attach UI.
 */

/**
 * Implements hook_ENTITY_TYPE_field_attach_ui().
 */
function entity_field_attach_ui_user_field_attach_ui($bundle, $field_name) {
  $form = array();
  // Form API code goes here...
  return $form;
}

/**
 * Implements hook_ENTITY_TYPE_hidden_field_disable().
 */
function entity_field_attach_ui_user_hidden_field_disable() {
  return array();
}